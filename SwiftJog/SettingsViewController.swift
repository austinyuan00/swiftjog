//
//  SettingsViewController.swift
//  SwiftJog
//
//  Created by Austin Yuan on 7/8/20.
//  Copyright © 2020 Austin Yuan. All rights reserved.
//

import UIKit
import CoreData

class SettingsViewController: UIViewController {

    @IBOutlet weak var unitController: UISegmentedControl!
    @IBOutlet weak var modeController: UISegmentedControl!
    @IBOutlet weak var totalDistance: UILabel!
    @IBOutlet weak var totalTime: UILabel!
    
    var delegate: UIViewController!
    var rootDelegate: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if mode
        {
            overrideUserInterfaceStyle = .dark
            modeController.selectedSegmentIndex = 1
        }
        else
        {
            overrideUserInterfaceStyle = .light
        }
        if units
        {
            unitController.selectedSegmentIndex = 1
        }
        if totals.count > 0
        {
            let curTotal = totals[0]
            let td = curTotal.value(forKeyPath: "totalDistance")!
            let tt = curTotal.value(forKeyPath: "totalTime")!
            let curDist = units ? td as! Double * convert : td as! Double
            totalDistance?.text = String(format: "%.2f ", curDist) + (units ? "km" : "miles")
            var hh = tt as! Int/3600 == 0 ? "00" : "\(tt as! Int/3600)"
            var mm = tt as! Int%3600/60 == 0 ? "00" : "\(tt as! Int%3600/60)"
            var ss = tt as! Int%3600%60 == 0 ? "00" : "\(tt as! Int%3600%60)"
            hh = hh.count == 1 ? "0" + hh : hh
            mm = mm.count == 1 ? "0" + mm : mm
            ss = ss.count == 1 ? "0" + ss : ss
            totalTime?.text = "\(hh):\(mm):\(ss)"
        }
        else
        {
            totalDistance.text = "0 " + (units ? "km" : "miles")
            totalTime.text = "00:00:00"
        }
    }
    
    
    @IBAction func changeUnits(_ sender: Any) {
        // miles
        if unitController.selectedSegmentIndex == 0
        {
            units = false
            if totals.count > 0 {
                let curTotal = totals[0]
                let td = curTotal.value(forKeyPath: "totalDistance")!
                let curDist = units ? td as! Double * convert : td as! Double
                totalDistance?.text = String(format: "%.2f ", curDist) + (units ? "km" : "miles")
            }
            else {
                totalDistance?.text =  "0 " + (units ? "km" : "miles")
            }
            if delegate is ResultsViewController {
                delegate.viewDidLoad()
            }
            
        }
        else
        {
            units = true
            if totals.count > 0 {
                let curTotal = totals[0]
                let td = curTotal.value(forKeyPath: "totalDistance")!
                let curDist = units ? td as! Double * convert : td as! Double
                totalDistance?.text = String(format: "%.2f ", curDist) + (units ? "km" : "miles")
            }
            else {
                totalDistance?.text =  "0 " + (units ? "km" : "miles")
            }
            if delegate is ResultsViewController {
                delegate.viewDidLoad()
            }
        }
        storeSettings()
    }
    
    @IBAction func changeMode(_ sender: Any) {
        if modeController.selectedSegmentIndex == 0
        {
            mode = false
            
        }
        else
        {
            mode = true
        }
        storeSettings()
        if mode
        {
            overrideUserInterfaceStyle = .dark
            delegate.overrideUserInterfaceStyle = .dark
            if delegate is ResultsViewController
            {
                rootDelegate.overrideUserInterfaceStyle = .dark
            }
            navigationController?.navigationBar.barTintColor = UIColor.darkGray
        }
        else
        {
            overrideUserInterfaceStyle = .light
            delegate.overrideUserInterfaceStyle = .light
            if delegate is ResultsViewController
            {
                rootDelegate.overrideUserInterfaceStyle = .light
            }
            navigationController?.navigationBar.barTintColor = UIColor.white
        }
    }
    
    func storeSettings() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let testRun = NSEntityDescription.insertNewObject(forEntityName: "Settings", into: context)
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Settings")
        var fetchedResults: [NSManagedObject]
        
        testRun.setValue(mode, forKey: "mode")
        testRun.setValue(units, forKey: "units")
        do {
            try fetchedResults = context.fetch(request) as! [NSManagedObject]
            if fetchedResults.count > 1
            {
                context.delete(fetchedResults[0])
            }
            try context.save()
        } catch {
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            abort()
        }
    }
    
    @IBAction func resetCoreData(_ sender: Any) {
        let controller = UIAlertController(
            title: "Reset Data Confirmation",
            message: "Are you sure you want to clear all data?",
            preferredStyle: .alert)
        controller.addAction(UIAlertAction(title:"Confirm", style:.destructive,
            handler: {
                (paramAction: UIAlertAction!) in self.confirmedReset()
        }))
        controller.addAction(UIAlertAction(title:"Cancel", style:.cancel,
            handler: nil))
        present(controller, animated: false, completion: nil)
    }
    
    func confirmedReset() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Totals")
        var fetchedResults: [NSManagedObject]
        
        do {
            try fetchedResults = context.fetch(request) as! [NSManagedObject]
            for r in fetchedResults
            {
                context.delete(r)
            }
            try context.save()
        } catch {
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            abort()
        }
        totals = []
        totalTime?.text = "00:00:00"
        totalDistance?.text = "0 " + (units ? "km" : "miles")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
