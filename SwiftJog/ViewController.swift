//
//  ViewController.swift
//  SwiftJog
//
//  Created by Austin Yuan on 6/19/20.
//  Copyright © 2020 Austin Yuan. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

var units = false // toggle units, false = miles, true = kilometers
var mode = false // toggle scheme, false = light, true = dark
let convert: Double = 1.60934 // kilometers in a mile
var stillRunning = false
var currentRun = Run()
let locManager = LocationManager.shared
var distance = Measurement(value: 0, unit: UnitLength.miles)
var locationList: [CLLocation] = []

var totals: [NSManagedObject] = []

class ViewController: UIViewController {

    @IBOutlet weak var currentTime: UILabel!
    
    func retrieveSettings() -> [NSManagedObject] {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Settings")
        var fetchedResults: [NSManagedObject]
        do {
            try fetchedResults = context.fetch(request) as! [NSManagedObject]
        } catch {
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            abort()
        }
        return(fetchedResults)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let settings = retrieveSettings()
        if settings.count > 0
        {
            mode = settings[0].value(forKeyPath: "mode") as! Bool
            units = settings[0].value(forKeyPath: "units") as! Bool
        }
        overrideUserInterfaceStyle = mode ? .dark : .light
        self.currentTime?.text = "00:00:00"
        // Do any additional setup after loading the view
        totals = retrieveData()
    }
    
    @IBAction func start(_ sender: Any) {
        if stillRunning
        {
            return
        }
        distance = Measurement(value: 0, unit: UnitLength.miles)
        locationList.removeAll()
        locManager.delegate = self
        // helps device be more optimized for this type of app
        locManager.activityType = .fitness
        // normalize readings
        locManager.distanceFilter = 10
        locManager.startUpdatingLocation()
        
        currentRun = Run()
        self.currentTime?.text = "00:00:00"
        stillRunning = true
        DispatchQueue.global(qos: .userInteractive).async {
            while stillRunning {
                sleep(1)
                self.callUpdate(currentRun: currentRun)
            }
        }
    }
    
    func callUpdate(currentRun: Run) {
        DispatchQueue.main.async {
            currentRun.update(newDist: distance.value * 0.000621371)
            let time = currentRun.timeInSeconds
            var hh = time/3600 == 0 ? "00" : "\(time/3600)"
            var mm = time%3600/60 == 0 ? "00" : "\(time%3600/60)"
            var ss = time%3600%60 == 0 ? "00" : "\(time%3600%60)"
            hh = hh.count == 1 ? "0" + hh : hh
            mm = mm.count == 1 ? "0" + mm : mm
            ss = ss.count == 1 ? "0" + ss : ss
            self.currentTime?.text = "\(hh):\(mm):\(ss)"
        }
    }
    
    @IBAction func seeProgress(_ sender: Any) {
        //maybe add feature if can think of one
    }
    
    // stop gathering information for this run, segue to results
    // update core data
    @IBAction func finish(_ sender: Any) {
        if stillRunning == false {
            return
        }
        stillRunning = false
        locManager.stopUpdatingLocation()
        let wasEmpty = totals.count > 0 ? false : true
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let testRun = NSEntityDescription.insertNewObject(forEntityName: "Totals", into: context)
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Totals")
        var fetchedResults: [NSManagedObject]
        
        if wasEmpty
        {
            testRun.setValue(currentRun.distance, forKey: "totalDistance")
            testRun.setValue(currentRun.timeInSeconds, forKey: "totalTime")
        }
        else
        {
            let curTotal = totals[0]
            let td = curTotal.value(forKeyPath: "totalDistance")!
            let tt = curTotal.value(forKeyPath: "totalTime")!
            testRun.setValue(td as! Double + currentRun.distance, forKey: "totalDistance")
            testRun.setValue(tt as! Int + currentRun.timeInSeconds, forKey: "totalTime")
        }
        do {
            if !wasEmpty {
                try fetchedResults = context.fetch(request) as! [NSManagedObject]
                context.delete(fetchedResults[0])
                totals[0] = testRun
            }
            else {
                totals.append(testRun)
            }
            try context.save()
        } catch {
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            abort()
        }
    }
    
    func retrieveData() -> [NSManagedObject] {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Totals")
        var fetchedResults: [NSManagedObject]
        do {
            try fetchedResults = context.fetch(request) as! [NSManagedObject]
        } catch {
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            abort()
        }
        return(fetchedResults)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "mainToSettings",
            let nextVC = segue.destination as? SettingsViewController {
                nextVC.delegate = self
            }
        else
        {
            let nextVC = segue.destination as? ResultsViewController
            nextVC?.delegate = self
        }
    }
    
}

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for newLocation in locations {
            let howRecent = newLocation.timestamp.timeIntervalSinceNow
            // check if device is confident its reading is within 20 meters of user's actual location
            // also data must be from < 10 seconds ago
            guard newLocation.horizontalAccuracy < 20 && abs(howRecent) < 10 else { continue }
            
            if let lastLocation = locationList.last {
                let delta = newLocation.distance(from: lastLocation)
                distance = distance + Measurement(value: delta, unit: UnitLength.meters)
            }
            locationList.append(newLocation)
        }
    }
}

