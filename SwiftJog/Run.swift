//
//  Run.swift
//  SwiftJog
//
//  Created by Austin Yuan on 7/8/20.
//  Copyright © 2020 Austin Yuan. All rights reserved.
//

import Foundation

class Run {
    var distance: Double
    var timeInSeconds: Int
    var averageSpeed: Double // units/hour
    
    init() {
        distance = 0.0
        timeInSeconds = 0
        averageSpeed = 0.0
    }
    
    func update(newDist: Double) {
        self.distance = newDist
        self.timeInSeconds += 1
        self.averageSpeed = distance/(Double(timeInSeconds)/3600.0)
    }
    
    
}
