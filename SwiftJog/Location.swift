//
//  Location.swift
//  SwiftJog
//
//  Created by Austin Yuan on 7/9/20.
//  Copyright © 2020 Austin Yuan. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager {
    static let shared = CLLocationManager()
    
    private init() { }
}


