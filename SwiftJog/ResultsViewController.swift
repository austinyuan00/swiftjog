//
//  ResultsViewController.swift
//  SwiftJog
//
//  Created by Austin Yuan on 7/8/20.
//  Copyright © 2020 Austin Yuan. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit


class ResultsViewController: UIViewController, MKMapViewDelegate {
    
    var delegate: UIViewController!
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var curTime: UILabel!
    @IBOutlet weak var avgSpeed: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        mapView.delegate = self
        if mode
        {
            overrideUserInterfaceStyle = .dark
        }
        else
        {
            overrideUserInterfaceStyle = .light
        }
        distance?.text = String(format: stillRunning ? "Current Distance: %.4f " : "Final Distance: %.4f ", units ? currentRun.distance * convert : currentRun.distance) + (units ? "km" : "miles")
        let time = currentRun.timeInSeconds
        var hh = time/3600 == 0 ? "00" : "\(time/3600)"
        var mm = time%3600/60 == 0 ? "00" : "\(time%3600/60)"
        var ss = time%3600%60 == 0 ? "00" : "\(time%3600%60)"
        hh = hh.count == 1 ? "0" + hh : hh
        mm = mm.count == 1 ? "0" + mm : mm
        ss = ss.count == 1 ? "0" + ss : ss
        curTime?.text = "\(stillRunning ? "Current Time: " : "Final Time: ") \(hh):\(mm):\(ss)"
        avgSpeed?.text = String(format: "Average Speed: %.2f ", units ? currentRun.averageSpeed * convert: currentRun.averageSpeed) + (units ? "km" : "miles") + "/hour"
        loadMap()
    }
    
    private func mapRegion() -> MKCoordinateRegion? {
      guard
        locationList.count > 0
      else {
        
        return nil
      }
        
      let latitudes = locationList.map { location -> Double in
        let location = location
        return location.coordinate.latitude
      }
        
      let longitudes = locationList.map { location -> Double in
        let location = location
        return location.coordinate.longitude
      }
        
      let maxLat = latitudes.max()!
      let minLat = latitudes.min()!
      let maxLong = longitudes.max()!
      let minLong = longitudes.min()!
        
      let center = CLLocationCoordinate2D(latitude: (minLat + maxLat) / 2,
                                          longitude: (minLong + maxLong) / 2)
      let span = MKCoordinateSpan(latitudeDelta: (maxLat - minLat) * 1.3,
                                  longitudeDelta: (maxLong - minLong) * 1.3)
      return MKCoordinateRegion(center: center, span: span)
    }
    
    private func polyLine() -> MKPolyline {

      let coords: [CLLocationCoordinate2D] = locationList.map { location in
        let location = location
        return CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
      }
      return MKPolyline(coordinates: coords, count: coords.count)
    }
    
    private func loadMap() {
      guard
        locationList.count > 0,
        let region = mapRegion()
      else {
          let alert = UIAlertController(title: "Error",
                                        message: "Sorry, this run has no locations saved",
                                        preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .cancel))
          present(alert, animated: true)
          return
      }
        
      mapView.setRegion(region, animated: true)
        mapView.addOverlay(polyLine())
    }


    internal func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
      guard let polyline = overlay as? MKPolyline else {
        return MKOverlayRenderer(overlay: overlay)
      }
      let renderer = MKPolylineRenderer(polyline: polyline)
      renderer.strokeColor = .black
      renderer.lineWidth = 3
      return renderer
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "resultToSettings",
            let nextVC = segue.destination as? SettingsViewController {
                nextVC.delegate = self
                nextVC.rootDelegate = self.delegate
            }
    }


}

//extension ResultsViewController: MKMapViewDelegate {
//  func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
//    guard let polyline = overlay as? MKPolyline else {
//      return MKOverlayRenderer(overlay: overlay)
//    }
//    let renderer = MKPolylineRenderer(polyline: polyline)
//    renderer.strokeColor = .black
//    renderer.lineWidth = 3
//    return renderer
//  }
//}
